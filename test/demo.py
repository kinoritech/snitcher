from snitcher import Snitcher


class Worker(Snitcher):
    def do_work(self, work):
        for w in work:
            print("working on {}".format(w))
        self.s_inform()  # Let agents know work is done


class Supervisor:
    def notify(self, *args, **kwargs):
        print("Worker done")


print("Simple")
w1 = Worker()
work = ["loadA", "loadB"]
boss = Supervisor()
w1.s_register_agent(boss)
w1.do_work(work)

from datetime import datetime
from snitcher import Scoop
import time


class WorkerScoop(Scoop):
    def __init__(self, type, started, finished):
        super().__init__(type)
        self.started = started
        self.finished = finished


class Worker(Snitcher):
    def do_work(self, work):
        scoop = None
        for w in work:
            start = datetime.now()
            print("working on {}".format(w))
            time.sleep(2)
            s = WorkerScoop(type="Work", started=start, finished=datetime.now())
            if scoop is None:
                scoop = s
            else:
                scoop.s_add(s)
        self.s_inform(scoop)


class Supervisor:
    def notify(self, scoop):
        info = [(str(s.started), str(s.finished)) for s in scoop]
        print("Worker done with: {}, details: {}".format(scoop.type, info))


print("\nCustom Scoops")
w1 = Worker()
work = ["loadA", "loadB"]
boss = Supervisor()
w1.s_register_agent(boss)
w1.do_work(work)


class Worker(Snitcher):
    def do_work(self, work):
        scoop = None
        for w in work:
            start = datetime.now()
            print("working on {}".format(w))
            time.sleep(2)
            s = WorkerScoop(type="Work", started=start, finished=datetime.now())
            if scoop is None:
                scoop = s
            else:
                scoop.s_add(s)
        self.s_inform(scoop)

    def do_break(self, duration):
        start = datetime.now()
        time.sleep(duration)
        s = WorkerScoop(type="Break", started=start, finished=datetime.now())
        self.s_inform(s)


class Supervisor:
    def supervise(self, scoop):
        info = [(str(s.started), str(s.finished)) for s in scoop]
        print("Worker done with: {}, details: {}".format(scoop.type, info))

print("\nCustom methods")

w1 = Worker()
work = ["loadA", "loadB"]
boss = Supervisor()
w1.s_register_agent(boss, boss.supervise)   # Provide the preferred notification method
w1.do_work(work)
w1.do_break(2)
work = ["loadC", "loadD"]
w1.do_work(work)
# Don't monitor breaks
w1.s_chatty = False
w1.do_break(2)
# Monitor back
w1.s_chatty = True
work = ["loadE", "loadF"]
w1.do_work(work)
