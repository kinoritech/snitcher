.. snitcher documentation master file, created by
   sphinx-quickstart on Sat Sep 30 23:16:25 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. role:: python(code)
    :language: python

Welcome to Snitcher's documentation!
====================================

.. include:: ../../README.rst
    :start-line: 13

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
