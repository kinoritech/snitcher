Changelog
=========
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[Unreleased]
------------


[0.2.1] - 2018-04-23
--------------------

Added
~~~~~
- Add tests to improve coverage.
- Completed the README, added example.
- Added *args and **kwargs to Snitcher init to improve inheritance support.
- Improved logging messages

[0.1.6] - 2017-08-27
--------------------
Initial release. Includes the first API implementation, tests and documentation.

.. Added
   ~~~~~
   Changed
   ~~~~~~~
   Fixed
   ~~~~~
   Removed
   ~~~~~~~
